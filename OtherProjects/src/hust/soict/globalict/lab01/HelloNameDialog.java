package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

/**
 *
 * @author thinh
 */
public class HelloNameDialog {
	public static void main(String[] args) {
		// TODO code application logic here
		String result;
		result = JOptionPane.showInputDialog("Please enter your name:");
		JOptionPane.showMessageDialog(null, "Hi " + result + "!");
		System.exit(0);
	}
}
