package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class simpleCaculation {
	public static void main(String[] args) {
		String strNum1, strNum2;
		String strNotification = "You've just entered: ";
		strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
				JOptionPane.INFORMATION_MESSAGE);
		strNotification += strNum1 + " and ";
		strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
				JOptionPane.INFORMATION_MESSAGE);
		strNotification += strNum2;
		double num1 = Double.parseDouble(strNum1);
		double num2 = Double.parseDouble(strNum2);
		double sum = num1 + num2, difference = num1 - num2, product = num1 * num2, quotient = num1 / num2;
		strNotification += "\n Sum of two number: " + sum;
		strNotification += "\n Difference of two number: " + difference;
		strNotification += "\n Product of two number: " + product;
		if (num2 == 0)
			strNotification += "\n Can not excecute equotient";
		else
			strNotification += "\n Quotient of two number: " + quotient;
		JOptionPane.showMessageDialog(null, strNotification, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
