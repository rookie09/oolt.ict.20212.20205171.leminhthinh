package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

/**
 *
 * @author thinh
 */
public class firstEquation {
	public static void main(String[] args) {
		String type;
		String strNotification = "You've just entered: \n";
		type = JOptionPane.showInputDialog(null,
				"Please choose type of equation! \n Enter 1 for first-degree equation \n Enter 2 for system of first-degree equation \n Enter 3 for second-degree euqation",
				"Input type", JOptionPane.INFORMATION_MESSAGE);
		double typeNum = Double.parseDouble(type);
		if (typeNum == 1) {
			String a, b;
			a = JOptionPane.showInputDialog(null, "Please input a: ", "Input a", JOptionPane.INFORMATION_MESSAGE);
			strNotification += a + "x + ";
			b = JOptionPane.showInputDialog(null, "Please input b: ", "Input b", JOptionPane.INFORMATION_MESSAGE);
			strNotification += b + " = 0";
			double a_num = Double.parseDouble(a);
			double b_num = Double.parseDouble(b);
			if (a_num == 0)
				strNotification += "\nCan not execute equation";
			else
				strNotification += "\nSolution of equation: " + (-b_num / a_num);
		}
		if (typeNum == 2) {
			String a11, a12, a21, a22, b1, b2;
			a11 = JOptionPane.showInputDialog(null, "Please input a11: ", "Input a11", JOptionPane.INFORMATION_MESSAGE);
			strNotification += a11 + "x1 + ";
			a12 = JOptionPane.showInputDialog(null, "Please input a12: ", "Input a12", JOptionPane.INFORMATION_MESSAGE);
			b1 = JOptionPane.showInputDialog(null, "Please input b1: ", "Input b1", JOptionPane.INFORMATION_MESSAGE);
			strNotification += a12 + " = " + b1 + "\n";
			a21 = JOptionPane.showInputDialog(null, "Please input a21: ", "Input a21", JOptionPane.INFORMATION_MESSAGE);
			strNotification += a21 + "x2 + ";
			a22 = JOptionPane.showInputDialog(null, "Please input a22: ", "Input a22", JOptionPane.INFORMATION_MESSAGE);
			b2 = JOptionPane.showInputDialog(null, "Please input b2: ", "Input b2", JOptionPane.INFORMATION_MESSAGE);
			strNotification += a22 + " = " + b2;
			double a11_num = Double.parseDouble(a11);
			double a12_num = Double.parseDouble(a12);
			double a21_num = Double.parseDouble(a21);
			double a22_num = Double.parseDouble(a22);
			double b1_num = Double.parseDouble(b1);
			double b2_num = Double.parseDouble(b2);
			double D = a11_num * a22_num - a21_num * a12_num;
			double D1 = b1_num * a22_num - b2_num * a12_num;
			double D2 = a11_num * b2_num - a21_num * b1_num;
			if (D == 0)
				strNotification += "\n The system has infinitely many solutions";
			if (D != 0)
				strNotification += "\n x1 = " + (D1 / D) + "\nx2 = " + (D2 / D);
			else
				strNotification += "\n The system has no solution";
		}
		if (typeNum == 3) {
			String a, b, c;
			a = JOptionPane.showInputDialog(null, "Please input a (a # 0): ", "Input a",
					JOptionPane.INFORMATION_MESSAGE);
			strNotification += a + "x^2 + ";
			b = JOptionPane.showInputDialog(null, "Please input b: ", "Input b", JOptionPane.INFORMATION_MESSAGE);
			strNotification += b + "x + ";
			c = JOptionPane.showInputDialog(null, "Please input c: ", "Input c", JOptionPane.INFORMATION_MESSAGE);
			strNotification += c + " = 0";
			double a_num = Double.parseDouble(a);
			double b_num = Double.parseDouble(b);
			double c_num = Double.parseDouble(c);
			double delta = b_num * b_num - 4 * a_num * c_num;
			if (delta == 0)
				strNotification += "\nSolution of equation: " + (-b_num) / (2 * a_num);
			if (delta > 0) {
				strNotification += "\nFirst solution of equation: " + (-b_num + Math.sqrt(delta)) / (2 * a_num);
				strNotification += "\nSecond solution of equation: " + (-b_num - Math.sqrt(delta)) / (2 * a_num);
			}
			if (delta < 0)
				strNotification += "\nEquation has no solution";
		} else
			strNotification += "No command match!!";
		JOptionPane.showMessageDialog(null, strNotification, "Equation", JOptionPane.INFORMATION_MESSAGE);
	}
}