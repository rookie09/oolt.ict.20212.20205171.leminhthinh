package hust.soict.globalict.lab02;

import java.util.Scanner;

/**
 *
 * @author
 */
public class InputFromKeyboard {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		System.out.println("What is your name?");
		String name = keyboard.nextLine();
		System.out.println("How old are you?");
		int age = keyboard.nextInt();
		System.out.println("How tall are you?");
		double height = keyboard.nextDouble();
		System.out.println("Mr " + name + ", age " + age + ", height " + height);
	}
}
