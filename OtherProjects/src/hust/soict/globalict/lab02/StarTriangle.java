package hust.soict.globalict.lab02;

import java.util.Scanner;

/**
 *
 * @author thinh
 */
public class StarTriangle {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner number = new Scanner(System.in);
		System.out.print("Enter number n: ");
		int n = number.nextInt();
		for (int i = 1; i <= n; i++) {
			for (int j = n - 1; j >= i; j--)
				System.out.print(" ");
			for (int k = 1; k <= 2 * i - 1; k++)
				System.out.print("*");
			System.out.print("\n");
		}
	}
}