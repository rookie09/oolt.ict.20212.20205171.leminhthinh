package hust.soict.globalict.lab02;

import java.util.Arrays;

public class matrixSum {
	public static void main(String[] args) {
		int[][] arr1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
		int[][] arr2 = { { 4, 6 }, { 7, 4 }, { 2, 5 } };
		int[][] sum = new int[3][2];
		for (int row = 0; row < arr1.length; row++)
			for (int col = 0; col < arr1[row].length; col++)
				sum[row][col] = arr1[row][col] + arr2[row][col];

		System.out.println(Arrays.deepToString(sum));
	}
}