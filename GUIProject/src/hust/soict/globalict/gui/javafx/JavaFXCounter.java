package hust.soict.globalict.gui.javafx;

import java.awt.Button;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class JavafxCounter extends Application {
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		tfCount = new TextField("0");
		tfCount.setEditable(false);
		btnCount = new Button("Count");
		btnCount.setOnAction(e -> tfCount.setText(++count + ""));

		FlowPane flow = new FlowPane();
		flow.setPadding(new Insets(15, 12, 15, 12));
		flow.setVgap(10);
		flow.setHgap(10);
		flow.setAlignment(Pos.CENTER);
		flow.getChildren().addAll(new Label("Count"), tfCount, btnCount);

		primaryStage.setScene(new Scene(flow, 400, 100));
		primaryStage.setTitle("JavaFX Counter");
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
