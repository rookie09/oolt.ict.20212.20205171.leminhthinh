package hust.soict.globalict.aims;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MyDate;

public class DiskTest {
	public static void main(String[] args) {
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);

		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);

		System.out.println(dvd1.search("The Lion King"));
		System.out.println(dvd1.search("Lion King Leo"));
		System.out.println(dvd1.search("Li"));

		MyDate date = new MyDate();

		Order order = new Order(date);
//		Order order1 = new Order(date);
//		Order order3 = new Order(date);
//		Order order4 = new Order(date);
//		Order order5 = new Order(date);
//		Order order6 = new Order(date);

		order.addDigitalVideoDisc(dvd1, dvd2);
		order.addDigitalVideoDisc(dvd3);

		order.printOrder();
		DigitalVideoDisc disc = order.getALuckyItem();
		System.out.println(disc.getTitle() + " is now a lucky item!");
		order.printOrder();
	}
}
