package hust.soict.globalict.aims.Aims;

import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;

public class Aims {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MemoryDaemon mDaemon = new MemoryDaemon();
		Thread thread = new Thread(mDaemon);
		thread.start();
		int choice;
		try (Scanner scanner = new Scanner(System.in)) {
			Order order = null;
			while (true) {
				showMenu();
				choice = scanner.nextInt();
				scanner.nextLine();
				switch (choice) {
				case 1 -> {
					if (Order.getNbOrders() == Order.MAX_NUMBERS_ORDERED) {
						System.out.println("Reach the limitation of number of orders!");
						break;
					}
					order = new Order();
					System.out.println("New order has been created");
				}

				case 2 -> {
					if (order != null) {
						System.out.print("Enter kind of item (Book or DVD or CD): ");
						String kind = scanner.nextLine();
						System.out.print("Enter title: ");
						String title = scanner.nextLine();
						System.out.print("Enter category: ");
						String category = scanner.nextLine();
						System.out.print("Enter cost: ");
						float cost = Float.parseFloat(scanner.nextLine());
						switch (kind) {
						case "DVD": {
							System.out.print("Please enter the length (in second) of your DVD: ");
							int length = scanner.nextInt();
							scanner.nextLine();
							DigitalVideoDisc item = new DigitalVideoDisc(title, category, cost, length);
							item.setId(order.getId() + "-D" + order.getNbItems());
							order.addMedia(item);
							System.out.print("Play DVD?(Y/N) ");
							if (scanner.nextLine().equalsIgnoreCase("Y"))
								item.play();
							break;
						}
						case "CD": {
							CompactDisc item = new CompactDisc(title, category, cost);
							item.setId(order.getId() + "-C" + order.getNbItems());
							System.out.print("Add tracks?(Y/N) ");
							while (scanner.nextLine().equalsIgnoreCase("Y")) {
								System.out.print("Enter track's title: ");
								String trTilte = scanner.nextLine();
								System.out.printf("Enter %s's length (in second): ", trTilte);
								int trLength = scanner.nextInt();
								scanner.nextLine();
								item.addTrack(new Track(trTilte, trLength));
								System.out.print("Continue?(Y/N) ");
							}
							order.addMedia(item);
							System.out.print("Play CD?(Y/N) ");
							if (scanner.nextLine().equalsIgnoreCase("Y"))
								item.play();
							break;
						}
						case "Book": {
							Book item = new Book(title, category, cost);
							item.setId(order.getId() + "-B" + order.getNbItems());
							order.addMedia(item);
							break;
						}
						default:
							System.out.println("Invalid kind!");
							break;
						}
					} else {
						System.out.println("No order has been created! Please choose 1 to create new order!!");
					}
				}
				case 3 -> {
					if (order != null) {
						System.out.print("Enter item id: ");
						String id = scanner.nextLine();
						order.removeMedia(id);
					} else {
						System.out.println("No order has been created! Please choose 1 to create new order!!");

					}
				}

				case 4 -> {
					if (order != null) {
						order.printOrder();
					} else {
						System.out.println("No order has been created! Please choose 1 to create new order!!");

					}
				}

				case 0 -> {
					System.out.println("Goodbye!!");
					return;
				}
				default -> System.out.println("Unexpected value: " + choice);
				}
			}
		}
	}

	public static void showMenu() {
		System.out.print("""
				Other Management Application:
				--------------------------------
				1. Create new order
				2. Add item to the order
				3. Delete item by id
				4. Display the items list of order
				0. Exit
				--------------------------------
				""");
		System.out.print("Please enter your choice (1-4): ");
	}
}
