package hust.soict.globalict.aims.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class MyDate {
	private int day, month, year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public void setDay(String day) {
		this.day = Integer.valueOf(day.replaceAll("th|st|nd|rd", ""));
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public void setMonth(String month) {
		this.month = Month.valueOf(month.toUpperCase()).getValue();
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public MyDate() {
		String dateString = LocalDate.now().toString();
		LocalDate curDate = LocalDate.parse(dateString);
		this.day = Integer.valueOf(curDate.getDayOfMonth());
		this.month = Integer.valueOf(curDate.getMonthValue());
		this.year = Integer.valueOf(curDate.getYear());

	}

	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public MyDate(String inputDate) {
		String[] dateStrings = inputDate.split(" ");
		this.year = Integer.valueOf(dateStrings[2]);
		this.month = Month.valueOf(dateStrings[0].toUpperCase()).getValue();
		this.day = Integer.valueOf(dateStrings[1].replaceAll("th|st|nd|rd", ""));
	}

	public MyDate(String day, String month, String year) {
		String[] dayStrings = { "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth",
				"tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth",
				"eighteenth", "nineteenth", "twentieth" };
		for (int i = 0; i < dayStrings.length; ++i) {
			if (day.equalsIgnoreCase(dayStrings[i])) {
				this.day = i + 1;
				break;
			} else if (day.equalsIgnoreCase("twenty-" + dayStrings[i])) {
				this.day = i + 21;
				break;
			}
		}
		if (day.equalsIgnoreCase("thirtieth"))
			this.day = 30;
		if (day.equalsIgnoreCase("thirty first"))
			this.day = 31;

		this.month = Month.valueOf(month.toUpperCase()).getValue();

		String[] yearStrings = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
				"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
				"twenty" };
		String[] yearStrings2 = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
		String[] strs = year.split(" ");

		if (strs.length == 1) {
			for (int i = 0; i < yearStrings.length; ++i) {
				if (strs[0].equalsIgnoreCase(yearStrings[i])) {
					this.year = i + 1;
					break;
				}
			}
		}

		if (strs.length == 2 || strs.length == 3) {
			int tmp = 1;
			for (int i = 0; i < yearStrings.length; ++i) {
				if (strs[0].equalsIgnoreCase(yearStrings[i])) {
					this.year = (i + 1) * 100;
					break;
				}
			}
			if (strs[1].equalsIgnoreCase("thousand")) {
				this.year *= 10;
			}
			if (strs[1].equalsIgnoreCase("oh")) {
				for (int i = 0; i < 9; ++i) {
					if (strs[2].equalsIgnoreCase(yearStrings[i])) {
						this.year += i + 1;
						tmp = 0;
						break;
					}
				}
			}
			if (tmp == 1) {
				int check = 1;
				for (int i = 0; i < yearStrings2.length; ++i) {
					if (strs[1].equalsIgnoreCase(yearStrings2[i])) {
						this.year += (i + 2) * 10;
						check = 0;
						break;
					}
					if (check == 1) {
						for (int j = 0; j < 9; ++j) {
							if (strs[1].equalsIgnoreCase(yearStrings2[i] + "-" + yearStrings[j])) {
								this.year += (i + 2) * 10 + j + 1;
								break;
							}
						}
					}
				}
			}
		}
	}

	public void accept() {
		System.out.print("Input date (MM/DD/YYYY) or American english words: ");

		try (Scanner input = new Scanner(System.in)) {
			String date = input.nextLine();
			String[] dateArr = date.split("/");
			this.setDay(dateArr[1]);
			this.setMonth(Integer.valueOf(dateArr[0]));
			this.setYear(Integer.valueOf(dateArr[2]));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

	}

	public void print() {
		switch (this.getDay()) {
		case 1, 21, 31:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " "
					+ this.getDay() + "st " + this.getYear());
			break;
		case 2, 22:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " "
					+ this.getDay() + "nd " + this.getYear());
			break;
		case 3, 23:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " "
					+ this.getDay() + "rd " + this.getYear());
			break;
		default:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " "
					+ this.getDay() + "th " + this.getYear());
			break;
		}
	}

	public String returnFormat(String form) {
		SimpleDateFormat stdFormat = new SimpleDateFormat("MM-dd-yyyy");
		Date date = null;
		try {
			date = stdFormat.parse("" + this.getMonth() + "-" + this.getDay() + "-" + this.getYear());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat formatter = new SimpleDateFormat(form);
		return formatter.format(date);
	}

	public void print(String form) {
		System.out.println(returnFormat(form));
	}
}
