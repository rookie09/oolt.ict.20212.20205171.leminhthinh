package hust.soict.globalict.aims.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	public static int dateCompare(MyDate date1, MyDate date2) {
		String dateFormat = "MM-dd-yyyy";
		SimpleDateFormat stdFormat = new SimpleDateFormat(dateFormat);
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = stdFormat.parse(date1.returnFormat(dateFormat));
			d2 = stdFormat.parse(date2.returnFormat(dateFormat));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (d1.compareTo(d2));
	}

	public static void sortDates(MyDate[] dateList) {
		for (int i = 0; i < dateList.length - 1; i++) {
			for (int j = i + 1; j < dateList.length; j++) {
				if (dateCompare(dateList[i], dateList[j]) > 0) {
					MyDate tmpDate = dateList[i];
					dateList[i] = dateList[j];
					dateList[j] = tmpDate;
				}
			}
		}
		for (MyDate date : dateList)
			date.print();
	}
}
