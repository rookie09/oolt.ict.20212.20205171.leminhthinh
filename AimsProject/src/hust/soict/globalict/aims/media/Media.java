package hust.soict.globalict.aims.media;

public abstract class Media {
	private String title;
	private String category;
	private float cost;
	private String id;
	private boolean lucky = false;

	public Media(String title) {
		this.title = title;
	}

	public Media(String title, String category) {
		this(title);
		this.category = category;
	}

	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public float getCost() {
		return cost;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isLucky() {
		return lucky;
	}

	public void setLucky(boolean lucky) {
		this.lucky = lucky;
	}

	@Override
	public boolean equals(Object object) {
		// TODO Auto-generated method stub
		if (this == object) {
			return true;
		}
		if (!(object instanceof Media)) {
			return false;
		}
		if (object.getClass() != this.getClass()) {
			return false;
		}
		Media media = (Media) object;
		if (this.id == null ? media.id != null : !this.id.equalsIgnoreCase(media.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return getTitle() + " - " + getCategory() + ": " + getCost() + "$";
	}

}
