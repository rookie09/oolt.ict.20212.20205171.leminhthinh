package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class BookTest {
	public static void main(String[] args) {
		Book book = new Book("lotr", "fantasy", 99);
		List<String> authors = new ArrayList<String>();
		authors.add("Tolkien");
		book.setAuthors(authors);
		book.setContent("a f g w j k u y r v n k l o g t c");
		System.out.println(book.toString());
	}

}
