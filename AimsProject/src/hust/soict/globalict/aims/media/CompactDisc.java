package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable<Object> {
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<>();

	public CompactDisc(String title, int length, String director) {
		super(title, length, director);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, int length) {
		super(title, category, cost, length);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, String artist, ArrayList<Track> tracks) {
		super(title, category, cost);
		this.artist = artist;
		this.tracks = tracks;
	}

	public String getArtist() {
		return artist;
	}

	public void addTrack(Track track) {
		if (!tracks.contains(track)) {
			tracks.add(track);
			System.out.println(track.getTitle() + " has been added");
		} else {
			System.out.println(track.getTitle() + " has been already in the list");
		}
	}

	public void removeTrack(Track track) {
		if (!tracks.contains(track)) {
			System.out.println(track.getTitle() + " is not in the list");
		} else {
			tracks.remove(track);
			System.out.println(track.getTitle() + " has been removed");
		}
	}

	public int getLength() {
		int length = 0;
		for (Track track : tracks) {
			length = track.getLength();
		}
		return length;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength() + "s");
		System.out.println("CD contains:");
		for (Track track : tracks) {
			track.play();
		}
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		CompactDisc other = (CompactDisc) o;
		if (tracks.size() != other.tracks.size()) {
			return tracks.size() - other.tracks.size();
		}
		return this.getLength() - other.getLength();
	}
}
