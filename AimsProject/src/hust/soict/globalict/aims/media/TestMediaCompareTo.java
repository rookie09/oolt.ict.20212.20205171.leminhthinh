package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestMediaCompareTo {
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		List<DigitalVideoDisc> discs = new ArrayList<>();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", 19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);

		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", 90.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);

		discs.add(dvd2);
		discs.add(dvd1);
		discs.add(dvd3);

		java.util.Iterator iterator = discs.iterator();
		System.out.println("--------------------------------------------------");
		System.out.println("The DVDs currently in the order are: ");

		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
		}

		Collections.sort(discs);
		iterator = discs.iterator();
		System.out.println("--------------------------------------------------");
		System.out.println("The DVDs in sorted order are: ");
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc) iterator.next()).getTitle());
		}
	}
}
