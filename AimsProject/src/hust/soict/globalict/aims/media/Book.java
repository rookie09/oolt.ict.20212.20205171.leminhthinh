package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media implements Comparable<Object> {
	private List<String> authors = new ArrayList<String>();

	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;

	public List<String> getAuthors() {
		return authors;
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public Book(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public Book(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public Book(String title, List<String> authors) {
		super(title);
		this.authors = authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public void addAuthor(String authorName) {
		if (!this.authors.contains(authorName))
			this.authors.add(authorName);
	}

	public void removeAuthor(String authorName) {
		if (this.authors.contains(authorName))
			for (int i = 0; i < this.authors.size(); i++)
				if (this.authors.get(i).equals(authorName))
					this.authors.remove(i);
	}

	public int compareTo(Object o) {
		Media other = (Book) o;
		if (this.getCost() < other.getCost())
			return -1;
		if (this.getCost() == other.getCost())
			return 0;
		return 1;
	}

	public void setContent(String content) {
		this.content = content;
		this.processContent();
	}

	public String getBasicInfo() {
		return super.toString();
	}

	public Map<String, Integer> processContent() {
		wordFrequency = new TreeMap<>();
		String[] arrayTokens = content.split(" ");

		for (String token : arrayTokens) {
			wordFrequency.put(token, wordFrequency.getOrDefault(token, 0) + 1);
			wordFrequency.putIfAbsent(token, 0);
		}

//		contentTokens = new ArrayList<String>(wordFrequency.keySet());
		return wordFrequency;
	}

	@Override
	public String toString() {
		String result = super.toString();
		result += "\n+ Content: " + content;
		result += "\n+ TokenList: " + contentTokens;
		result += "\n+ Frequency: " + wordFrequency;

		return result;
	}
}
