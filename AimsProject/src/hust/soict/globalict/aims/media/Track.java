package hust.soict.globalict.aims.media;

public class Track implements Playable, Comparable<Object> {
	private String title;
	private int length;

	public Track(String title) {
		super();
		this.title = title;
	}

	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public int getLength() {
		return length;
	}

	@Override
	public void play() {
		System.out.println("Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength() + "s");
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Track)) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		Track track = (Track) obj;
		if (this.title == null ? track.title != null : !this.title.equalsIgnoreCase(track.title)) {
			return false;
		}
		if (this.length != track.length) {
			return false;
		}
		return true;
	}

	public int compareTo(Object o) {
		Track other = (Track) o;
		if (this.getLength() < other.getLength())
			return -1;
		if (this.getLength() == other.getLength())
			return 0;
		return 1;
	}

}
