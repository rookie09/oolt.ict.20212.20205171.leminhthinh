package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Object> {

	public DigitalVideoDisc(String title, int length, String director) {
		super(title, length, director);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category, float cost, int length) {
		super(title, category, cost, length);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public boolean search(String title) {
		String[] titleString = title.toLowerCase().replaceAll("\\s+", " ").split(" ");
		for (int i = 0; i < titleString.length; ++i) {
			if (!this.getTitle().toLowerCase().contains(titleString[i]))
				return false;
		}
		return true;
	}

	@Override
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(Object o) {
		Media other = (DigitalVideoDisc) o;
		if (this.getCost() < other.getCost())
			return -1;
		if (this.getCost() == other.getCost())
			return 0;
		return 1;
	}

}
