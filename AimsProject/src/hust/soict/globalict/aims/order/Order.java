package hust.soict.globalict.aims.order;

import java.util.ArrayList;
import java.util.Random;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	public static final int MAX_NUMBERS_LUCKY_ITEMS = 1;
	private static int nbOrders = 0;

	private MyDate dateOrdered;

	private ArrayList<Media> itemsOrdered = new ArrayList<>();
	private static int nbLuckyItems = 0;
	private String id;

	public Order(MyDate d) {
		if (nbOrders == MAX_LIMITTED_ORDERS) {
			System.out.println("Can not make more than " + MAX_LIMITTED_ORDERS + " orders!");
			this.dateOrdered = null;
		} else {
			this.dateOrdered = d;
			id = "ORD" + nbOrders;
			nbOrders++;
		}
	}

	public Order() {
		this(new MyDate());
	}

	public static int getNbOrders() {
		return nbOrders;
	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void addMedia(Media media) {
		if (itemsOrdered.size() == MAX_LIMITTED_ORDERS) {
			System.out.println("The order list if full");
		} else {
			if (itemsOrdered.contains(media))
				System.out.println(media.getTitle() + " has been already added in the list");
			else {
				itemsOrdered.add(media);
				System.out.println(media.getTitle() + " has been added");
			}
		}
	}

	public void removeMedia(String id) {
		int check = 0;
		for (int i = 0; i < itemsOrdered.size(); i++)
			if (itemsOrdered.get(i).getId().equals(id)) {
				check = 1;
				itemsOrdered.remove(i);
				System.out.println("Have removed!");
				break;
			}
		if (check == 0) {
			System.out.println("The item is not on the order list!");
		}
	}

	public float totalCost() {
		float sum = 0;
		for (Media item : itemsOrdered) {
			if (item.isLucky() == false)
				sum += item.getCost();
		}
		return sum;
	}

	public String getId() {
		return id;
	}

	public void printOrder() {
		System.out.println("\n***********************Order***********************");
		System.out.print("Date: ");
		this.getDateOrdered().print();
		System.out.println("Ordered items:");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (itemsOrdered == null)
				break;
			System.out.println((i + 1) + ". " + itemsOrdered.get(i).getId() + " - " + itemsOrdered.get(i).getTitle()
					+ " - " + itemsOrdered.get(i).getCategory() + " - " + ": " + itemsOrdered.get(i).getCost() + "$");
			if (itemsOrdered.get(i).isLucky())
				System.out.print(itemsOrdered.get(i).getTitle() + " - Lucky item");
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************");
	}

	public Media getALuckyItem() {
		if (nbLuckyItems == itemsOrdered.size() || nbLuckyItems == MAX_NUMBERS_LUCKY_ITEMS)
			return null;
		Random r = new Random();
		int randInt = r.nextInt(itemsOrdered.size());
		if (itemsOrdered.get(randInt).isLucky() == false) {
			itemsOrdered.get(randInt).setLucky(true);
			nbLuckyItems++;
			return itemsOrdered.get(randInt);
		} else {
			return getALuckyItem();
		}
	}

	public int getNbItems() {
		return this.itemsOrdered.size();
	}

}