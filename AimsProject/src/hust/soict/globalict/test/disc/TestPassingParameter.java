package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;

class dvdWrapper {
	DigitalVideoDisc dvd;

	dvdWrapper(DigitalVideoDisc dvd) {
		this.dvd = dvd;
	}
}

class TestPassingParameter {
	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");

		dvdWrapper dvdWrapper1 = new dvdWrapper(jungleDVD);
		dvdWrapper dvdWrapper2 = new dvdWrapper(cinderellaDVD);

		swap(dvdWrapper1, dvdWrapper2);
		System.out.println("Jungle: " + dvdWrapper1.dvd.getTitle());
		System.out.println("Cinderella: " + dvdWrapper2.dvd.getTitle());

		changeTitle(jungleDVD, cinderellaDVD.getTitle());
		System.out.println("Jungle: " + jungleDVD.getTitle());
	}

	public static void swap(dvdWrapper o1, dvdWrapper o2) {
		DigitalVideoDisc tmp = o1.dvd;
		o1.dvd = o2.dvd;
		o2.dvd = tmp;
	}

	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
}
