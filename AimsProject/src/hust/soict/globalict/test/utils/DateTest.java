package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate day1 = new MyDate();

		MyDate day2 = new MyDate(30, 12, 2002);

		MyDate day3 = new MyDate("January 1st 2003");

		MyDate day6 = new MyDate("sixteenth", "September", "two sixty-one");

		MyDate day4 = new MyDate();
		day4.setDay("18");
		day4.setMonth("May");
		day4.setYear(2002);

//		MyDate day5 = new MyDate();
//		day5.accept();

		System.out.print("Day 1: ");
		day1.print();
		System.out.print("Day 2: ");
		day2.print();
		System.out.print("Day 3: ");
		day3.print();
		System.out.print("Day 4: ");
		day4.print();
//		System.out.print("Day 5: ");
//		day5.print();
		System.out.print("Day 6: ");
		day6.print();
	}

}
